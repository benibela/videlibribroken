<?xml version="1.0" encoding="UTF-8"?>
<actions>


<meta>
  <description>Template für das Sisis-System</description>
  <variable name="server"><description>Serveraddresse (inklusive Protokoll)</description></variable>
  <variable name="startparams" default=""><description>Parameter für den Aufruf des Web-OPACs, falls mehrere Bibliotheken auf dem Server existieren (inklusive "?")</description></variable>
</meta>

<action id="catalogue">
  <variable name="url" value="{$server}/start.do{get('startparams','')}"/>
</action>

<action id="connect">
  <s>touchpoint := contains($server, "touchpoint")</s>
  <page url="{$server}/start.do{get('startparams','')}" templateFile="start"/>
</action>

<action id="login">
  <page url="{$server}/login.do;{$jsession}">
    <post name="methodToCall" value="submit"/>
    <post name="CSId" value="{$CSId}"/>
    <post name="username" value="{$username}"/>
    <post name="password" value="{$password}"/>
    
    <template><t:switch prioritized="true">
    <body>
    <a t:optional="true">deutsch<t:if test="/html/@lang != 'de'">
        <t:read var="raise()" source="concat('VideLibri funktioniert nur, wenn die Sprache des Webkatalogs auf deutsch gesetzt ist.', $line-ending, 'Die Einstellung kann auf der normalen Katalogseite unter Konto / Sucheinstellungen / Sprache geändert werden.', $line-ending, $line-ending, $line-ending, 'VideLibri only works, if the language of the library web catalog is set to German. ', $line-ending, ' You can change the language setting on the normal web page on the  Account / Search preferences / Language page.')"/>
    </t:if></a>
    <form id="LoginBean" t:optional="true">
      <t:switch>
      <div class="error"><t:read var="raise-login()" source="."/></div>
      <span class="error"><t:read var="raise-login()" source="."/></span>
      <div class="message-warning"><t:read var="raise-login()" source="."/></div>
      </t:switch>
    </form>
    </body>
    <table class="data" id="holdings_"></table> <!-- touch point sends us to the last AJAX request -->
    </t:switch>
    </template>
  </page>
</action>

<action id="update-all">
  <call action="login"/>
  
  <s>requestedUrl := "",
     orderedUrl := "",
     startUrl := x'{$server}/userAccount.do;jsession;?methodToCall={if ($touchpoint) then "start" else "show&amp;typ=1"}',
     minRenewedId := $minCancelId := "ZZZZZZZZ",
     nextPage := ""
  </s>
  
  <page url="{$startUrl}"  templateFile="loggedIn"></page>
  <loop test="$nextPage != ''">
    <page url="{$nextPage}"  templateFile="loggedIn"></page>
  </loop>

  <variable name="startUrl">resolve-uri($requestedUrl)</variable>
  <page url="{$startUrl}"  templateFile="loggedIn"></page>
  <loop test="$nextPage != ''">
    <page url="{$nextPage}"  templateFile="loggedIn"></page>
  </loop>

  <variable name="startUrl">resolve-uri($orderedUrl)</variable>
  <page url="{$startUrl}"  templateFile="loggedIn"></page>
  <loop test="$nextPage != ''">
    <page url="{$nextPage}"  templateFile="loggedIn"></page>
  </loop>
  <!--  <error templateFile="loginError"/>-->
</action>


<action id="renew-list">
  <s>list := for $book in $renew-books order by $book.actionURI descending return $book, need-update := false()</s>
  <if test="$list[1].actionURI ge $minRenewedId">
    <call action="update-all"/>
    <s>list := for $book in $list ! vl:select-book(.) order by $book.actionURI descending return $book</s>
  </if>
  <loop var="b" list="$list">
    <page url="{$b._onPageStart}"/> <!-- only possible  -->
    <page test="$b._onPage != $b._onPageStart" url="{$b._onPage}"/> <!-- todo: test if this is needed as well -->
    
    <page url="{$b.actionURI}" templateFile="singleExtended"/>
  </loop>
  <s>let $x := ($list[count($list)].actionURI) return if ($x lt $minRenewedId) then $minRenewedId := $x else ()</s>
  <if test="$need-update"><call action="update-all"/></if>
</action>

<action id="cancel-list">
  <s>list := for $book in $cancel-books order by $book.actionURI descending return $book</s>
  <if test="$list[1].actionURI ge $minCancelId">
    <call action="update-all"/>
    <s>list := for $book in $list ! vl:select-book(.) order by $book.actionURI descending return $book</s>
  </if>
  <loop var="b" list="$list">
    <page url="{$b._onPageStart}"/> <!-- only possible  -->
    <page test="$b._onPage != $b._onPageStart" url="{$b._onPage}"/> <!-- todo: test if this is needed as well -->
    
    <page url="{$b.actionURI}"/>
    <!--  <template>      	 <span class="textgruen"><strong>Stornierung durchgeführt</strong></span></br></template>-->
  </loop>
  <s>let $x := ($list[count($list)].actionURI) return if ($x lt $minCancelId) then $minCancelId := $x else ()</s>
</action>



<!--<action id="renew-all">
  <page url="{$server}/AusleiheVerlaengerungServlet;{$jsession}" templateFile="KontoServlet">
    <post name="Kontoart" value="Ausleihkonto"/>
    <post name="Bibliothek" value="111"/>
    <post name="homegkz" value="111"/>
  </page>
</action>-->

<action id="search-connect">
  <call action="connect"/>
  <s>$home-branches := $search-branches := (), $home-branch-id := $search-branch-id := 0</s>
</action>


<action id="search">

  <s>
  oldSearchOptionCount := get("searchOptionCount", 0),
  searchOptions := 
    (if ($book.title)    then [if ($touchpoint) then "200" else "331", ($book.title)]    else (),
     if ($book.author)   then ["100", ($book.author)]   else (),
     if ($book.isbn)     then [if ($touchpoint) then "490" else "540", ($book.isbn)]     else (),
     if ($book.year)     then [if ($touchpoint) then "280" else "425", ($book.year)]     else (),
     if ($book.keywords) then [if ($touchpoint) then "50" else "902", ($book.keywords)] else ()),
  searchOptionCount := count($searchOptions),
  searchUrl :=  uri-combine( 
    x"{$server}/search.do?methodToCall=submit&CSId={$CSId}&methodToCallParameter=submitSearch", (
    { "searchCategories[0]": $searchOptions[1](1), "searchString[0]": $searchOptions[1](2) },
    if ($searchOptionCount > 1 or $oldSearchOptionCount > 1) then 
      { "combinationOperator[1]": "AND", "searchCategories[1]": $searchOptions[2](1), "searchString[1]": $searchOptions[2](2) } else (),
    if ($searchOptionCount > 2 or $oldSearchOptionCount > 2) then
      { "combinationOperator[2]": "AND", "searchCategories[2]": $searchOptions[3](1), "searchString[2]": $searchOptions[3](2) } else (),
    {"submitSearch": "Suchen", "callingPage": "searchParameters"},
    if (exists($home-branches[$home-branch-id])) then  
      {$home-branch-name: $home-branch-values[$home-branch-id]} else (),
    if (exists($search-branches[$search-branch-id])) then   
      ($search-branch-extended-options, {$search-branch-name: $search-branch-values[$search-branch-id]}) else ()
    )
  )
  </s>


  <s>$nextPage:=$singleBookResult:=speedPage:=""</s>
  
  <page url="{$searchUrl}" templateFile="searchList"/>
  <page test="$speedPage != ''" url="{$speedPage}" templateFile="searchList"/>

  <page test="$singleBookResult != ''"  url="{$singleBookResult}" templateFile="searchSingle"/>
  
  <variable name="search-next-page-available">$nextPage != ''</variable>
</action>

<action id="search-next-page">
  <page url="{$nextPage}" templateFile="searchList"></page>
  <variable name="search-next-page-available">$nextPage != ''</variable>
</action>

<action id="search-details">
  <if test="$touchpoint">
    <page url="{$book._detail-url}" templateFile="searchSingle"/>
    <page url="{replace($book._detail-url,'(.*methodToCall)=[^&amp;]+(&amp;.*)identifier=(.*)', '$1=showHoldings$2hitlistIdentifier=$3')}" templateFile="searchSingleTPShowHolding"/>
    <page url="{replace($book._detail-url,'(.*methodToCall)=[^&amp;]+(&amp;.*)identifier=(.*)', '$1=showDocument$2hitlistIdentifier=$3')}" templateFile="searchSingleTPDocument"/>
    <!--{$server}/singleHit.do?methodToCall=showDocument&ajax=true-->
  </if>
  <if test="not($touchpoint)">
    <page url="{$book._detail-url}&amp;tab=showTitleActive" templateFile="searchSingle"/>
    <page url="{$book._detail-url}&amp;tab=showAvailabilityActive" templateFile="searchSingleAvailability"/>
    <page url="{$book._detail-url}&amp;tab=showExemplarActive" templateFile="searchSingleExemplar"/>
  </if>
</action>



<action id="order-single"> 
  <if test="not(get('loggedIn', false()))"> 
    <call action="login"/>
    <variable name="loggedIn">true()</variable>
  </if>
  <s>confirm := "", finish-order-now := false(), choose-result:=1,
     url := $book(concat('order', $book.choosenOrder)), 
     post := $book(concat('orderPost', $book.choosenOrder)),
     if ($post) then url := {"url": $url, "post": $post, "method": "POST"} else ()
    </s>
  <page url="{$url}"  templateFile="orderConfirmation"/>
  <call test="$finish-order-now" action="internal-order"/>
</action>

<action id="internal-order-confirmed">  
  <call test="$confirm-result" action="internal-order"/>  
</action>

<action id="internal-order">
  <if test="$choose-result ne 0">
    <if test="not($choose-result instance of xs:decimal)">
      <variable name="confirm">
      {
        "url": $confirm.url,
        "method": $confirm.method,
        "post": replace(replace($confirm.post, "issuepoint=", concat("issuepoint=", $choose-result )) (: old sisis :),
                   "location=", concat("location=", $choose-result)) (: touchpoint :)
      }
      </variable>  
    </if>
    <page url="{$confirm}">
      <template>
        <t:switch prioritized="true">
        <form t:condition="contains(@action, 'acknowledge.do') ">
         <!-- ok -->
  <!--    <template>          Das Medium wurde für Sie vorgemerkt
    und ist voraussichtlich bis |  Das Medium wurde für Sie an 2. Stelle vorgemerkt.  
    
     Ausgabeort: Zentralbibliothek    </template>-->
        </form>   
        <div class="message-confirm"/>
        </t:switch>   
      </template>
    </page>
  </if>
</action>

</actions> 
